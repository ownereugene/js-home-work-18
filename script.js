let obj1 = {
    students: {
        name1: "Nikolay",
        name2: "Katya",
        name3: "Nikita",
        name4: "Eugene",
        name5: "Misha",
        name6: "Djon",
        name7: "Serge",
        name8: "Sonya",
        name9: "Dave",
        name10: "Jenya",
    },
    lessons: {
        lesson1: "CSS",
        lesson2: "HTML",
        lesson3: "JS",
    },
    marks: [1, 2, 3, 4, 5],
    courses: [1, 2, 3, 4],
}



function cloneObj(obj) {
    let clone = {}
    for (let key in obj) {
        if (typeof obj[key] == "object") {
            clone[key] = cloneObj(obj[key])
        } else {
            clone[key] = obj[key]
        }
    }
    return clone
}

console.log(cloneObj(obj1))